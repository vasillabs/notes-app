if (process.env.NODE_ENV === 'production') {
    module.exports = { 
        mongoURI: 'mongodb://vasilhristov_1:654321a@ds263740.mlab.com:63740/orgranizer-prod'
    }
} else {
    module.exports = {
        mongoURI: 'mongodb://localhost/video-dev'
    }
}