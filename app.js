const express = require('express');
const path = require('path');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const methodOverride = require('method-override');

const app = express();

const ideas = require('./routes/ideas');
const users = require('./routes/users');

// Passport config 
require('./config/passport')(passport);

const db = require('./config/db');

// DB connection
mongoose.connect(db.mongoURI)
.then(() => console.log("MongoDB connected..."))
.catch((err) => console.log(err))


// Handlebar's middleware
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine','handlebars');

// Body parser middleware 
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Static folder 
app.use(express.static(path.join(__dirname, 'public')));

// method override middleware
app.use(methodOverride('_method'));

// session middleware
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}))

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// connect flash middleware
app.use(flash());

// Global variables
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error');   
    res.locals.user = req.user || null;
    next();
})

// Routesoutes
app.get('/', (req, res) => {
    const title = 'Welcome';
    res.render('index', {   
        title: title
    });
});

app.get('/about', (req, res) => {
    res.render('about');
})

// use routes
app.use('/ideas', ideas);

// use users
app.use('/users', users);

let serverPort = 5000;

const port = process.env.PORT || serverPort;

app.listen(port, function () {
    console.log(`The app is running on port ${port}`);    
})